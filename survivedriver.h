#ifndef SURVIVEDRIVER_H
#define SURVIVEDRIVER_H

#include <QObject>
#include <survive.h>
#include <survive_api.h>

struct vector3d
{
    float x;
    float y;
    float z;
};

class SurviveDriver : public QObject // must be QThread
{
    Q_OBJECT
public:
    explicit SurviveDriver(QObject *parent = nullptr);
    ~SurviveDriver();

signals:
    void origin_reset();
    void controller_pos_changed(QString ctrl,float x,float y,float z);

private slots:
    void timer_func();
private:
    void handle_pose_event(QString device,FLT timecode,const SurvivePose& pose);
    void handle_button_event(QString device,const struct SurviveSimpleButtonEvent *button_event,SurviveObjectSubtype subtype);
    vector3d left_ctrl;
    vector3d right_ctrl;
    vector3d origin;
    bool system_button_pressed[2];
    bool have_vr_origin=false;
};

#endif // SURVIVEDRIVER_H
