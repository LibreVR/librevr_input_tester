#include "playarea.h"
#include <QPainter>
#include <QDebug>

PlayArea::PlayArea(QWidget *parent) : QWidget(parent)
{

}

void PlayArea::reset()
{
    lpath.clear();
    rpath.clear();
}

void PlayArea::update(QString name, float x, float y)
{
    if(name=="left")
    {
        l[0] = x;
        l[1] = y;
        lpath.push_back(x);
        lpath.push_back(y);
    }
    else
    {
        r[0] = x;
        r[1] = y;
        rpath.push_back(x);
        rpath.push_back(y);
    }
    valid=true;
    QWidget::update();
}

void PlayArea::paintEvent(QPaintEvent *event)
{
    if(!valid) return;
    QPainter p(this);
    float xo=this->size().width()/2;
    float yo=this->size().height()/2;
    float xs=xo/5;
    float ys=xo/5;

    p.fillRect(QRect(xo-5+l[0]*xs,yo-5+l[1]*ys,10,10),QBrush(Qt::blue));
    p.fillRect(QRect(xo-5+r[0]*xs,yo-5+r[1]*ys,10,10),QBrush(Qt::red));

    p.setPen(Qt::blue);
    for(int i=0;i<lpath.size()-2;i+=2)
    {
        p.drawLine(xo-5+lpath[i+0]*xs,
                   yo-5+lpath[i+1]*ys,
                   xo-5+lpath[i+2]*xs,
                   yo-5+lpath[i+3]*ys);
    }
    p.setPen(Qt::red);
    for(int i=0;i<rpath.size()-2;i+=2)
    {
        p.drawLine(xo-5+rpath[i+0]*xs,
                   yo-5+rpath[i+1]*ys,
                   xo-5+rpath[i+2]*xs,
                   yo-5+rpath[i+3]*ys);
    }

}
