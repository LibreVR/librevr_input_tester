#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    playArea = new PlayArea();
    this->setCentralWidget(playArea);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::origin_reset()
{
    playArea->reset();
}
void MainWindow::controller_pos_changed(QString ctrl,float x,float y,float z)
{
    playArea->update(ctrl,x,y);
}
