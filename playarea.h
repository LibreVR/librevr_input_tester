#ifndef PLAYAREA_H
#define PLAYAREA_H

#include <QWidget>

class PlayArea : public QWidget
{
    float l[2];
    float r[2];
    QList<float> lpath;
    QList<float> rpath;
    bool valid=false;
    Q_OBJECT
public:
    explicit PlayArea(QWidget *parent = nullptr);

    void update(QString name,float x,float y);
    void reset();

protected:
    void paintEvent(QPaintEvent *event);
signals:

};

#endif // PLAYAREA_H
