#include "mainwindow.h"
#include "survivedriver.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SurviveDriver d;

    MainWindow w;
    w.connect(&d,&SurviveDriver::origin_reset,&w,&MainWindow::origin_reset);
    w.connect(&d,&SurviveDriver::controller_pos_changed,&w,&MainWindow::controller_pos_changed);
    w.show();
    return a.exec();
}
