//#define SURVIVE_ENABLE_FULL_API

#include "survivedriver.h"
#include <QTimer>
#include <QDebug>

//SurviveContext *ctx = nullptr;
SurviveDriver* drv=nullptr;
SurviveSimpleContext *actx=nullptr;

static void log_fn(SurviveSimpleContext *actx, SurviveLogLevel logLevel, const char *msg) {
    fprintf(stderr, "[LOG_FN](%7.3f) SimpleApi: %s\n", survive_simple_run_time(actx), msg);
    (void) logLevel; //TODO: toGUI
}

SurviveDriver::SurviveDriver(QObject *parent) : QObject(parent)
{
    if(drv) { qDebug() << "SurviveDriver is a singleton" ; abort(); }
    actx = survive_simple_init_with_logger(0, nullptr, log_fn);
    //ctx  = survive_simple_get_ctx(actx);

   // double start_time = OGGetAbsoluteTime();
   survive_simple_start_thread(actx);

   for (const SurviveSimpleObject *it = survive_simple_get_first_object(actx); it != 0;
        it = survive_simple_get_next_object(actx, it)) {
        printf("Found '%s'\n", survive_simple_object_name(it));
   }

    drv = this;

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &SurviveDriver::timer_func);
    timer->start(1);

    system_button_pressed[0]=false;
    system_button_pressed[1]=false;
}

SurviveDriver::~SurviveDriver()
{
    //survive_close(ctx);
}

bool show_button_events=false;

void SurviveDriver::handle_pose_event(QString device,FLT timecode,const SurvivePose& pose)
{
    if(device=="KN0")
    {
        //qDebug() << "pose_event_from_left";
        left_ctrl.x = pose.Pos[0];
        left_ctrl.y = pose.Pos[1];
        left_ctrl.z = pose.Pos[2];
    }
    if(device=="KN1")
    {
        right_ctrl.x = pose.Pos[0];
        right_ctrl.y = pose.Pos[1];
        right_ctrl.z = pose.Pos[2];
        //qDebug() << "pose_event_from_right";
    }
    (void) device;
    (void) timecode;
    (void) pose;
    //timecode, pose.Pos[0], pose.Pos[1], pose.Pos[2],
    //pose.Rot[0], pose.Rot[1], pose.Rot[2], pose.Rot[3]
}

void SurviveDriver::handle_button_event(QString device, const struct SurviveSimpleButtonEvent *button_event, SurviveObjectSubtype subtype)
{
    int index=0;
    if(device=="KN0" || device=="KN1")
    {
        if(device=="KN1") index=1;
        if (button_event->button_id != 255) {


            if (button_event->button_id == SURVIVE_BUTTON_SYSTEM && button_event->event_type==SURVIVE_INPUT_EVENT_BUTTON_DOWN) {
                FLT v = 1 - survive_simple_object_get_input_axis(button_event->object, SURVIVE_AXIS_TRIGGER);
                survive_simple_object_haptic(button_event->object, 30, v, .5);
                qDebug() << "ff" << device;
                system_button_pressed[index]=true;

            }
            else if(button_event->event_type==SURVIVE_INPUT_EVENT_BUTTON_DOWN)
            {
                qDebug() << "button press" << device << button_event->button_id;
            }
        }
    }
}


void SurviveDriver::timer_func()
{
    struct SurviveSimpleEvent event; memset(&event,0,sizeof(event));
    survive_simple_wait_for_event(actx, &event);
    switch (event.event_type) {
    case SurviveSimpleEventType_PoseUpdateEvent: {
        const struct SurviveSimplePoseUpdatedEvent *pose_event = survive_simple_get_pose_updated_event(&event);
        SurvivePose pose = pose_event->pose;
        FLT timecode = pose_event->time;
        auto name = survive_simple_object_name(pose_event->object);
        handle_pose_event(QString::fromUtf8(name),timecode,pose);
        break;
    }
    case SurviveSimpleEventType_ButtonEvent: {
        const struct SurviveSimpleButtonEvent *button_event = survive_simple_get_button_event(&event);
        SurviveObjectSubtype subtype = survive_simple_object_get_subtype(button_event->object);
        auto name = survive_simple_object_name(button_event->object);
        handle_button_event(QString::fromUtf8(name),button_event,subtype);
        break;
    }
    case SurviveSimpleEventType_ConfigEvent: {
        const struct SurviveSimpleConfigEvent *cfg_event = survive_simple_get_config_event(&event);
        printf("[CFG](%f) %s received configuration of length %u type %d-%d\n", cfg_event->time,
               survive_simple_object_name(cfg_event->object), (unsigned)strlen(cfg_event->cfg),
               survive_simple_object_get_type(cfg_event->object),
               survive_simple_object_get_subtype(cfg_event->object));
        break;
    }
    case SurviveSimpleEventType_DeviceAdded: {
        const struct SurviveSimpleObjectEvent *obj_event = survive_simple_get_object_event(&event);
        printf("(%f) Found '%s'\n", obj_event->time, survive_simple_object_name(obj_event->object));
        break;
    }
    case SurviveSimpleEventType_None:
    case SurviveSimpleEventType_Shutdown:
        break;
    }
    if(system_button_pressed[0] && system_button_pressed[0])
    {
        qDebug() << "set vr origin";
        origin.x = (left_ctrl.x + right_ctrl.x)/2;
        origin.y = (left_ctrl.y + right_ctrl.y)/2;
        origin.z = (left_ctrl.z + right_ctrl.z)/2;
        have_vr_origin = true;
        system_button_pressed[0]=false;
        system_button_pressed[1]=false;
        emit origin_reset();
    }
    if(have_vr_origin)
    {
        // qDebug() << "left" << (left_ctrl.x - origin.x) << (left_ctrl.y - origin.y) << (left_ctrl.z - origin.z);
        // qDebug() << "right" << (right_ctrl.x - origin.x) << (right_ctrl.y - origin.y) << (right_ctrl.z - origin.z);
        emit controller_pos_changed("left",(left_ctrl.x - origin.x),(left_ctrl.y - origin.y),(left_ctrl.z - origin.z));
        emit controller_pos_changed("right",(right_ctrl.x - origin.x),(right_ctrl.y - origin.y),(right_ctrl.z - origin.z));
    }
}



